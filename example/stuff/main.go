package main

import (
	"fmt"

	"gitlab.com/koishi/gotemple/example/stuff/inttmpl"
	"gitlab.com/koishi/gotemple/example/stuff/stringtmpl"
)

//go:generate gotemple init --package tmpl --dir tmpl --prefix Type

//go:generate gotemple build -i tmpl string
//go:generate gotemple build -i tmpl int

func main() {
	{
		fmt.Println("======[ strings ]=======")
		slice := stringtmpl.NewSlice()

		slice.Push("Hello")
		slice.Push("World")
		slice.Push("....")

		size := slice.Length()
		for i := 0; i < size; i++ {
			fmt.Println(slice.Pop())
		}
	}
	{
		fmt.Println("=======[ ints ]=========")
		slice := inttmpl.NewSlice()

		slice.Push(0)
		slice.Push(1)
		slice.Push(2)
		slice.Push(999)

		size := slice.Length()
		for i := 0; i < size; i++ {
			fmt.Println(slice.Pop())
		}
	}
}
