package inttmpl

// Slice ...
type Slice struct {
	data []int
}

// NewSlice returns a pointer to a new slice
func NewSlice() *Slice {
	return &Slice{
		data: []int{},
	}
}

// Push ...
func (s *Slice) Push(i int) int {
	s.data = append(s.data, i)
	return i
}

// Data ...
func (s *Slice) Data() []int {
	return s.data
}

// Length ...
func (s *Slice) Length() int {
	return len(s.data)
}

// Pop ...
func (s *Slice) Pop(i ...int) int {
	var index int
	if len(i) != 0 {
		index = i[0]
	} else {
		index = len(s.data) - 1
	}

	t := s.data[index]
	s.data = append(s.data[:index], s.data[index+1:]...)
	return t
}

// Shift ...
func (s *Slice) Shift() int {
	t := s.data[0]
	s.data = s.data[1:]
	return t
}
