package gotemple

import (
	"errors"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/fatih/astrewrite"
)

const typesFile = "types.gotemple.go"

// Build parses files in from sourceDir and
// outputs them to destDir
//    sourceDir : directory to parse source files
//    destDir   : directory to output modified files
//    types     : string of types to replace. type numbers
//                will correspond to indecies in this slice
func Build(sourceDir, destDir, packageName string, types []string) error {
	fset := token.NewFileSet()
	a, err := parser.ParseDir(fset, sourceDir, nil, parser.ParseComments)
	if err != nil {
		return err
	}

	if sourceDir == "" || sourceDir == "." {
		sourceDir, err = os.Getwd()
		if err != nil {
			return err
		}
	}

	if destDir == "" || destDir == "." {
		sourcePackage := filepath.Base(sourceDir)
		destDir = filepath.Join(filepath.Dir(sourceDir), strings.Join(types, "")+sourcePackage)
	}

	if packageName == "" {
		packageName = filepath.Base(destDir)
	}

	var (
		prefix string
	)

	// Find the types file to get the prefix used
	var foundPrefix bool
loopstart:
	for _, pkg := range a {
		for fname, file := range pkg.Files { // Rewrite types file with modified types
			if strings.HasSuffix(fname, typesFile) {
				prefix, _ = getTypesData(fset, file)
				foundPrefix = true
				break loopstart
			}
		}
	}
	if !foundPrefix {
		return errors.New("Could not extract type prefix")
	}

	for _, pkg := range a {
		for fname, file := range pkg.Files { // Rewrite types file with modified types
			path := filepath.Join(destDir, filepath.Base(fname))
			if strings.HasSuffix(fname, typesFile) { // Ignore types file
				continue
			} else {
				err = writeNodeToFile(path, fset, AlterTypes(fset, file, sourceDir, packageName, prefix, types))
			}
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// AlterTypes renames the types of type declaration files. Index 0 in the types slice will correspond to
// type0 and index 1 to type1 and so on
//    prefix      : prefix assigned to type names
//    node        : node to scan through
//    fset        : token FileSet
//    types       : slice of type names. Type numbers will correspond
//                  to indices in in the slice.
//    sourceDir   : sourceDirectory used to determine whether the file is in the base folder
//    packageName : new package name for generated files
func AlterTypes(fset *token.FileSet, node ast.Node, sourceDir, packageName string, prefix string, types []string) ast.Node {
	if types == nil {
		// Default to some integer types if none
		// have been specified
		types = []string{"int", "int", "int", "int"}
	}

	rewritten := astrewrite.Walk(node, func(n ast.Node) (ast.Node, bool) {
		switch t := n.(type) {
		case *ast.Ident:
			index := typeIndex(t.Name, prefix)
			if index >= 0 && index < len(types) {
				t.Name = types[index]
			}
		case *ast.File:
			t.Name.Name = packageName
		}
		return n, true
	})

	return rewritten
}

// typeIndex returns the index of the type by name
func typeIndex(name, prefix string) int {
	if str := regexp.MustCompile("(" + prefix + ")([0-9]+)").FindStringSubmatch(name); str != nil {
		n, _ := strconv.Atoi(str[2])
		return n
	}
	return -1
}

func writeNodeToFile(path string, fset *token.FileSet, node ast.Node) error {
	os.MkdirAll(filepath.Dir(path), 0600)
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer f.Close()
	return printer.Fprint(f, fset, node)
}

// getTypesData returns info stored in the comments of the types file
func getTypesData(fset *token.FileSet, file *ast.File) (prefix string, numTypes int) {
	for _, v := range file.Comments {
		t := v.Text()
		if str := regexp.MustCompile("(Prefix: )(.*)(\\s|$)").FindStringSubmatch(t); len(str) >= 2 {
			prefix = strings.TrimSpace(str[2])
		}
		if str := regexp.MustCompile("(NumTypes: )(.*)(\\s|$)").FindStringSubmatch(t); len(str) >= 2 {
			n, _ := strconv.Atoi(str[2])
			numTypes = n
		}
	}
	return
}
