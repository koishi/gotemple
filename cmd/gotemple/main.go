package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/koishi/gotemple"

	"github.com/alecthomas/kingpin"
)

var (
	app = kingpin.New("gotemple", "some tool to create template packages")

	// INIT
	cmdInit        = app.Command("init", "create types.gotemple.go")
	cmdInitPackage = cmdInit.Flag("package", "package name of generated type file").Default("main").Short('n').String()
	cmdInitPrefix  = cmdInit.Flag("prefix", "prefix of generated type").Default("_T").Short('p').String()
	cmdInitDir     = cmdInit.Flag("dir", "directory of output files").Short('o').String()

	// BUILD
	cmdBuild        = app.Command("build", "build a template folder")
	cmdBuildDir     = cmdBuild.Flag("dir", "input directory for files").Short('i').Required().String()
	cmdBuildDest    = cmdBuild.Flag("dest", "destination folder").Short('o').String()
	cmdBuildPackage = cmdBuild.Flag("package", "package name of generated file").Short('p').String()
	cmdBuildTypes   = cmdBuild.Arg("types", "list of types to modify").Strings()
)

func initHandler() {
	src, err := gotemple.CreateTypesFile(*cmdInitPackage, 20, *cmdInitPrefix)
	if err != nil {
		fmt.Println(string(src))
		log.Fatal(err)
	}
	if *cmdInitDir != "" {
		os.MkdirAll(*cmdInitDir, 0600)
	}
	ioutil.WriteFile(filepath.Join(*cmdInitDir, "types.gotemple.go"), src, 0600)
}

func buildHandler() {
	err := gotemple.Build(*cmdBuildDir, *cmdBuildDest, *cmdBuildPackage, *cmdBuildTypes)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	command, err := app.Parse(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}

	switch command {

	// Create types
	case cmdInit.FullCommand():
		initHandler()
	case cmdBuild.FullCommand():
		buildHandler()
	default:
		fmt.Println(app.Help)
	}
}
