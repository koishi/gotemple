package util

// Iterator helps templates iterate over a value
func Iterator(start, end int) chan int {
	loop := make(chan int)
	go func() {
		for i := start; i < end; i++ {
			loop <- i
		}
		close(loop)
	}()
	return loop
}
